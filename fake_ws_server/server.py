#!/usr/bin/env python

# pacman -S python-websockets python-msgpack
# ./server.py

import asyncio
import websockets
import msgpack
import random
from datetime import datetime

MAX_PIXEL_POOL = 5
WRONG_TOKEN_PROBABILITY = 0.05	# The rest are correct tokens
PIXEL_CLICK_PROBABILITY = 0.8	# The rest are pixel pool increments

class NotAuthorizedError(Exception):
	pass

class Game:
	def __init__(self, size):
		self.size = size
		self.board = [[0] * self.size[0] for _ in range(self.size[1])]
		self.colors = [
			{'id': 0, 'hexa': 'ffffff'},	# White
			{'id': 1, 'hexa': '000000'},	# Black
			{'id': 2, 'hexa': 'cc0000'},	# Red
			{'id': 3, 'hexa': '00cc00'},	# Green
			{'id': 4, 'hexa': '0000cc'},	# Blue
			{'id': 5, 'hexa': 'cccc00'},	# Yellow
			{'id': 6, 'hexa': '00cccc'},	# Cyan
			{'id': 7, 'hexa': 'cc00cc'},	# Magenta
		][:random.randint(3, 7)]

	def random_pixel(self):
		return {
			'x': random.randint(0, self.size[0] - 1),
			'y': random.randint(0, self.size[1] - 1),
			'color': self.colors[
				random.randint(0, len(self.colors) - 1)
			]['id'],
			'timestamp': datetime.now().isoformat(),
		}
	
	async def get_board(self, ws):
		await ws.send(msgpack.packb({
			'req': 'board',
			'size': {
				'x': self.size[0],
				'y': self.size[1]
			},
			'board': self.board,
			'colors': self.colors,
			'timestamp': datetime.now().isoformat(),
			'checksum': None,
		}))

	def click(self, x, y, color, timestamp):
		self.board[y][x] = color

class User:
	def __init__(self, ws):
		self.auth = False
		self.ws = ws
		self.pixel_pool = 1
	
	def authorize(self, token):
		if token is None or random.random() < WRONG_TOKEN_PROBABILITY:
			print('User not authorized')
			return False
		else:
			print('User authorized')
			self.auth = True
			return True
	
	async def update(self, x, y, color, timestamp):
		await self.ws.send(msgpack.packb({
			'req': 'update',
			'x': x, 'y': y, 'color': color,
			'timestamp': datetime.now().isoformat()
		}))
	
	async def send_pixel_pool(self):
		await self.ws.send(msgpack.packb({
			'req': 'pixel_pool',
			'count': self.pixel_pool
		}))

# Global state
game = Game((random.randint(6, 10) * 10, random.randint(4, 8) * 10))
users = set()


# Ticking coroutine for periodic events
async def ticker():
	while True:
		if random.random() < PIXEL_CLICK_PROBABILITY:
			# Simulate pixel update to all users
			pixel = game.random_pixel()
			print(f'Simulating pixel click ({pixel["x"]}, {pixel["y"]})')
			game.click(**pixel)
			for u in list(users):
				await u.update(**pixel)
		else:
			# Increment pixel pool of all users
			print('Incrementing pixel pools')
			for u in list(users):
				if u.pixel_pool < MAX_PIXEL_POOL:
					u.pixel_pool += 1
					await u.send_pixel_pool()

		await asyncio.sleep(random.randint(6, 20))

# Serve an individual message from user
async def serve(ws, user, msg):
	req = msg.get('req')

	if req == 'auth':
		print('Incoming auth request')
		is_auth = user.authorize(msg.get('token'))
		if is_auth:
			await game.get_board(ws)
			await user.send_pixel_pool()
		else:
			raise NotAuthorizedError
	elif req == 'click':
		print(f'Incoming click request ({msg.get("x")}, {msg.get("y")})')
		ts = datetime.now().isoformat()
		if (user.pixel_pool > 0):
			game.click(msg.get('x'), msg.get('y'), msg.get('color'), ts)
			for u in list(users):
				if u is not user:
					print('-> user')
					await u.update(msg.get('x'), msg.get('y'), msg.get('color'), ts)
			user.pixel_pool -= 1
			await user.send_pixel_pool()
	elif req == 'get_board':
		print('Incoming get_board request')
		await game.get_board(ws)
	else:
		print('Invalid request type: {msg}')


async def main(ws, path):
	print('New connection')

	user = User(ws)
	users.add(user)

	print(f'{len(users)} connected users')

	try:
		while True:
			data = await ws.recv()

			msg = None
			try:
				msg = msgpack.unpackb(data)
			except TypeError as ex:
				print(f'Invalid msgpack: {data}')

			if msg:
				await serve(ws, user, msg)
	except websockets.exceptions.ConnectionClosedOK:
		pass
	except NotAuthorizedError:
		pass
	finally:
		print('Connection closed')
		users.remove(user)
		print(f'{len(users)} connected users')


start_server = websockets.serve(main, "localhost", 8765)
evloop = asyncio.get_event_loop()
evloop.run_until_complete(start_server)
asyncio.ensure_future(ticker())

try:
	evloop.run_forever()
except KeyboardInterrupt:
	pass
finally:
	print("Quitting")
