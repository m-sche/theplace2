# The place 2

[[_TOC_]]

## Directory structure

* `db` – Database related files
    * `model.dbm` – Database model for [pgModeler](https://pgmodeler.io/). Can be used to update live database schema instead of dropping and recreating.
    * `create.sql` – PostgreSQL create script generated from `model.dbm`.
    * `insert_old.sql` – Data from years 2019, 2020 and a few events in game `dev1` intended for development.
* `server` – The Rust backend responsible for communication with clients over websocket and with database.
* `ui` – Client-facing interface. It connects to the server via websocket. It can be used standalone without any webserver, just open `index.html` and enable javascript.
* `fake_ws_server` – Simple server for frontend debugging. Multiple clients may connect via websocket on port 8765 at the same time and some events are randomly generated even if no other client is connected. External dependencies: `websockets`, `msgpack` python packages.
* `fake_ui` – Event viewer which connects to `ws://localhost:8765` and displays in/out msgpack events. It sends random `click` and `get_board` events every few seconds. Uses lightweight [ygoe/msgpack.js](https://github.com/ygoe/msgpack.js) library.


## Websocket API

### `click`

Client to server. Sent when user clicks the board and draws a pixel.

<details>
<summary>Click to view details</summary>

Parameters:

* `x` – X coordinate
* `y` – Y coordinate
* `color` – ID of the color

Example:
```json
{"req": "click", "x": 45, "y": 12, "color": 1}
```
</details>

### `get_board`

Client to server. Sent when client loses track of current state of the board, e.g. if the checksum doesn't match. It is not necessary to send `get_board` at the beginning of the session, since the server should send the board immediately after successful authorization.

<details>
<summary>Click to view details</summary>

No parameters.

Example:
```json
{"req": "get_board"}
```
</details>

### `board`

Server to client. Containts the board size and its current content. Sent after successful authorization or as a response to a `get_board` request.

<details>
<summary>Click to view details</summary>

Parameters:

* `size` – Object
    * `x` – Board width,
    * `y` – Board height
* `board` – 2D array of color IDs (list of board rows)
* `colors` – List of objects
    * `id` – ID of the color
    * `hexa` – Hexadecimal RGB representation of the color without a leading `#` character
* `background` - ID of the background color
* `timestamp` – Server time of the board state in ISO 8601 format
* `checksum` – Board state checksum / `null`

Example:
```json
{
    "req": "board",
    "size": {"x": 3, "y": 4},
    "board": [
        [0, 0, 0],
        [0, 1, 2],
        [0, 2, 0],
        [1, 0, 0]
    ],
    "colors": [
        {"id": 0, "hexa": "ffffff"},
        {"id": 1, "hexa": "000000"},
        {"id": 2, "hexa": "ee002c"}
    ],
    "timestamp": "2021-05-01T12:00:00.000",
    "checksum": null
}
```
</details>

### `update`

Server to client. Notifies users about newly drawn pixel. Should not be sent to the user who drew the pixel.

<details>
<summary>Click to view details</summary>

Parameters:

* `x` – X coordinate
* `y` – Y coordinate
* `color` – ID of the color
* `timestamp` – Server time of the change in ISO 8601 format
* `checksum` – Board state checksum / `null`

Example:
```json
{"req": "update", "x": 59, "y": 5, "color": 6, "checksum": null}
```
</details>

### `pixel_pool`

Server to client. Sent every time the size of user's pixel pool changes. Should be also sent immediately after authorization.

<details>
<summary>Click to view details</summary>

Parameters:

* `count` – New number of pixels available

Example:
```json
{"req": "pixel_pool", "count": 4}
```
</details>

### `error`

Request to display an error or warning to the user.

<details>
<summary>Click to view details</summary>

Parameters:

* `msg` – Text of the error message.

Example:
```json
{"req": "error", "msg": "Try again."}
```
</details>

### `get_events`

Client to server. Request a history of all click events in a form of `update` requests.

<details>
<summary>Click to view details</summary>

No parameters.

Example:
```json
{"req": "get_events"}
```
</details>

### `animation_end`

Server to client. Confirms that the client received all requested `update`s.

<details>
<summary>Click to view details</summary>

No parameters.

Example:
```json
{"req": "animation_end"}
```
</details>


## Database quickstart

1. Install postgres 12 and run it
2. Connect to the database:
    * `psql -U postgres`
    * `-U` specifies the db username, `postgres` is the default user with full permissions
    * Specify `-h localhost` if you want to use TCP instad of unix socket or if connecting to another server
3. Create the database in the interactive shell
    * `CREATE DATABASE theplace;`
    * `\c theplace` to switch to the newly created database
    * From now on, you can also connect directly using `psql -U postgres theplace`
4. Create the tables using the create script
    * `\i db/create.sql`
5. Fill in the old data
    * `\i db/insert_old.sql`
6. Profit
    * `\q` to quit the postgres shell
    * `\dt` to list the tables
    * `\?` to show help

You can dump the database (to refresh `db/insert_old.sql`) by
```shell
pg_dump -a -U postgres -h localhost theplace > db/insert_old.sql
```
Try to keep the sequence numbers unincremented if you added and deleted some rows locally.
