use anyhow::{anyhow, Context};
use async_std::prelude::*;
use chrono::DateTime;
use chrono::Utc;
use rmp_serde::from_read_ref;
use serde::{Deserialize, Serialize};
use std::fmt::Display;
use std::time::Duration;
use thiserror::Error;
use tide_websockets::{Message, WebSocketConnection};

use crate::bail;
use crate::db::{self, DATABASE};
use crate::server;

type Result<T, E = Error> = core::result::Result<T, E>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Connection closed")]
    Closed,
    #[error("The game has not started yet")]
    TooSoon,
    #[error("The game has already ended")]
    TooLate,
    #[error(transparent)]
    Database(#[from] db::Error),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

pub async fn run(
    mut stream: &mut WebSocketConnection,
    server_state: &server::ServerState,
    token: &str,
    stream_id: u32,
) -> Result<()> {
    let mut user_list = server_state.users.write().await;
    let user = user_list
        .get_mut(token)
        .expect("Unauthorized user started a game");
    let pixel_pool = user.pixel_pool;

    std::mem::drop(user_list); // TODO refactor all user_list usages into impl ServerState

    send_by_token(
        &Request::PixelPool { count: pixel_pool },
        server_state,
        token,
        None,
    )
    .await?;

    // Client wants its state ASAP
    send_request(
        &Request::Board(server_state.game.read().await.clone()),
        stream,
    )
    .await?;

    // if concurrency inside one connection is required
    // stream.try_for_each_concurrent
    loop {
        let request = receive_request(&mut stream).await;
        if let Err(Error::Closed) = request {
            break;
        }
        let request = request.context("Communication error")?;

        handle_request(stream, request, server_state, &token, stream_id).await?;
    }

    Ok(())
}

pub async fn keep_pinging(server_state: server::ServerState) -> Result<()> {
    let interval = Duration::from_secs(server_state.config.ping_interval);

    // TODO use stream::interval, when stabilized
    // pending https://github.com/async-rs/async-std/pull/885
    // but that's almost a year old and quite garbage
    loop {
        async_std::task::sleep(interval).await;
        // can't use send_everyone, because that only allows Requsets and this is a Message
        for user in server_state.users.write().await.values_mut() {
            for stream in user.streams.values_mut() {
                stream
                    .send(Message::Ping(Vec::new()))
                    .await
                    .context("Can't send WS ping")?;
            }
        }
    }
}

pub async fn keep_sending_pixels(server_state: server::ServerState) -> Result<()> {
    let interval = Duration::from_secs(server_state.config.pixel_interval);

    // TODO use stream::interval, when stabilized
    // pending https://github.com/async-rs/async-std/pull/885
    // but that's almost a year old and quite garbage
    loop {
        async_std::task::sleep(interval).await;

        let timestamp = Utc::now();
        let (start, end) = server_state.game.read().await.timespan;
        if let Some(start) = start {
            if timestamp < start {
                continue;
            }
        }
        if let Some(end) = end {
            if timestamp > end {
                send_everyone(&Request::PixelPool { count: 0 }, &server_state, None).await?;
                break;
            }
        }

        for user in server_state.users.write().await.values_mut() {
            if user.pixel_pool < server_state.config.pixel_limit {
                user.pixel_pool += 1;

                send_to_user(
                    &Request::PixelPool {
                        count: user.pixel_pool,
                    },
                    user,
                    None,
                )
                .await?;
            }
        }
    }

    Ok(())
}

// wait for binary request and answer any pings with pongs
async fn receive_request(stream: &mut WebSocketConnection) -> Result<Request> {
    loop {
        match stream.next().await {
            Some(Ok(Message::Binary(input))) => {
                let req: Request = match from_read_ref(&input) {
                    Err(err) => {
                        bail!("Received invalid request: {}", err);
                    }
                    Ok(req) => req,
                };

                return Ok(req);
            }
            // Supposedly automatically handled by the middleware, but I don't trust it, and pings
            // are being received.
            // https://github.com/http-rs/tide-websockets/issues/11#issuecomment-774295185
            Some(Ok(Message::Ping(payload))) => {
                stream
                    .send(Message::Pong(payload))
                    .await
                    .context("Can't respond to ping with pong")?;
                println!("Pong!"); // a bit weird, because our client supposedly doesn't do that
            }
            Some(Ok(Message::Pong(_))) => {
                // not tracking whether the pong is solicited or not
                // unsolicited pong is permitted by the spec as a unidirectional heartbeat
                // https://datatracker.ietf.org/doc/html/rfc6455#section-5.5.3
            }
            Some(Ok(Message::Close(_))) => {
                return Err(Error::Closed);
            }
            Some(Ok(_)) => {
                bail!("Received non-binary data");
            }
            Some(Err(err)) => {
                bail!("There was an error with the WebSocket connection: {}", err);
            }
            None => {
                bail!("No request was received");
            }
        }
    }
}

/// Sends {req: "error"} message over websocket to the client
async fn send_error(msg: impl Display, stream: &mut WebSocketConnection) -> Result<()> {
    send_request(
        &Request::Error {
            msg: msg.to_string(),
        },
        stream,
    )
    .await
}

async fn send_request(req: &Request, stream: &mut WebSocketConnection) -> Result<()> {
    let ret = stream
        .send_bytes(rmp_serde::to_vec_named(req).expect("Error serializing the WS request"))
        .await
        .context("Can't send WS request");

    Ok(ret?)
}

// send request to everyone except optionally specified stream_id
async fn send_everyone(
    req: &Request,
    server_state: &server::ServerState,
    except: Option<u32>,
) -> Result<()> {
    for user in server_state.users.write().await.values_mut() {
        send_to_user(req, user, except).await?;
    }

    Ok(())
}

// send request to all streams of a specified user except optionally specified stream_id
async fn send_by_token(
    req: &Request,
    server_state: &server::ServerState,
    token: &str,
    except: Option<u32>,
) -> Result<()> {
    let mut user_list = server_state.users.write().await;
    let user = user_list
        .get_mut(token)
        .ok_or_else(|| anyhow!("Attempt to send request to unknown user"))?;

    send_to_user(req, user, except).await
}

// send request to all streams of a specified user except optionally specified stream_id
async fn send_to_user(req: &Request, user: &mut server::User, except: Option<u32>) -> Result<()> {
    for stream in user.streams.iter_mut() {
        let (current_id, stream) = stream;
        // I sure hope rust has working loop hoisting
        if let Some(except) = except {
            if *current_id == except {
                continue;
            }
        }

        send_request(req, stream).await?;
    }

    Ok(())
}

async fn handle_request(
    stream: &mut WebSocketConnection,
    req: Request,
    server_state: &server::ServerState,
    token: &str,
    stream_id: u32,
) -> Result<()> {
    match req {
        // TODO try to get rid of clone
        Request::GetBoard => {
            send_request(
                &Request::Board(server_state.game.read().await.clone()),
                stream,
            )
            .await
        }
        Request::Click { x, y, color } => {
            let mut user_list = server_state.users.write().await;
            let user = user_list
                .get_mut(token)
                .ok_or_else(|| anyhow!("Unknown user sent a Click request"))?; // TODO can this be an expect?
            if user.pixel_pool == 0 {
                // the client is probably just out of sync, so let's not make a big deal out of it
                send_error("You have no pixels left", stream).await?;
                send_request(&Request::PixelPool { count: 0 }, stream).await?;
                undo_pixel(x, y, stream, server_state).await?;

                return Ok(());
            }
            user.pixel_pool -= 1;
            send_to_user(
                &Request::PixelPool {
                    count: user.pixel_pool,
                },
                user,
                Some(stream_id),
            )
            .await?;

            std::mem::drop(user_list);

            // intermediate value to drop the write lock fast
            // (lock locked in the value part of match lives until the end of the match)
            let res = server_state
                .game
                .write()
                .await
                .set_pixel(x, y, color, token)
                .await;
            let (timestamp, checksum) = match res {
                Ok(val) => val,
                Err(err @ (Error::TooSoon | Error::TooLate)) => {
                    send_error(err, stream).await?;
                    undo_pixel(x, y, stream, server_state).await?;

                    return Ok(());
                }
                Err(err) => return Err(err),
            };

            send_everyone(
                &Request::Update {
                    x,
                    y,
                    color,
                    timestamp,
                    checksum: Some(checksum),
                },
                server_state,
                Some(stream_id),
            )
            .await
        }
        Request::GetEvents => {
            let db = DATABASE.read().await;
            let events = db.get_events().await?;

            let mut time_diff_ms = vec![0 as u64];
            time_diff_ms.extend(
                events
                    .windows(2)
                    .map(|evs| (evs[1].timestamp - evs[0].timestamp).num_milliseconds() as u64),
            );

            send_request(
                &Request::Events {
                    count: events.len(),
                    x: events.iter().map(|ev| ev.x as u32).collect(),
                    y: events.iter().map(|ev| ev.y as u32).collect(),
                    color: events.iter().map(|ev| ev.color as u32).collect(),
                    time_first: events[0].timestamp,
                    time_diff_ms,
                },
                stream,
            )
            .await?;

            Ok(())
        }
        _ => bail!("Invalid request"),
    }
}

async fn undo_pixel(
    x: u32,
    y: u32,
    stream: &mut WebSocketConnection,
    server_state: &server::ServerState,
) -> Result<()> {
    let game_state = server_state.game.read().await;
    send_request(
        &Request::Update {
            x,
            y,
            color: game_state.board[y as usize][x as usize],
            timestamp: Utc::now(),
            checksum: Some(game_state.checksum()),
        },
        stream,
    )
    .await
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct GameState {
    size: BoardSize,
    board: Vec<Vec<u32>>,
    colors: Vec<Color>,
    background: u32,
    timestamp: DateTime<Utc>,
    #[serde(skip)]
    row_checksums: Vec<u32>,
    pub timespan: (Option<DateTime<Utc>>, Option<DateTime<Utc>>),
}
impl GameState {
    pub async fn new() -> Result<GameState> {
        let db = DATABASE.read().await;
        let colors = db.get_colors().await?;
        let board = db.get_board().await?;
        let timespan = db.get_game_timespan();

        let size = BoardSize {
            x: board.size.x as u32,
            y: board.size.y as u32,
        };
        let timestamp = board.timestamp;
        let board: Vec<Vec<u32>> = board
            .board
            .iter()
            .map(|x| x.iter().map(|&y| y as u32).collect())
            .collect();

        let row_checksums = board.iter().map(|row| Self::crc32_u32(row)).collect();

        let background = colors.background as u32;
        let colors = colors
            .colors
            .iter()
            .map(|color| Color {
                id: color.id as u32,
                hexa: color.hexa.clone(),
            })
            .collect();

        // this is really inefficient
        // probably could be fixed by sharing and unifying some types (like game/db::color and i/u32)
        let ret = GameState {
            size,
            board,
            colors,
            background,
            timestamp,
            row_checksums,
            timespan,
        };

        Ok(ret)
    }

    async fn set_pixel(
        &mut self,
        x: u32,
        y: u32,
        color: u32,
        token: &str,
    ) -> Result<(DateTime<Utc>, u32)> {
        if x >= self.size.x || y >= self.size.y {
            bail!("Requested out of bounds access to the board");
        }

        let timestamp = Utc::now();
        if let Some(start) = self.timespan.0 {
            if timestamp < start {
                return Err(Error::TooSoon);
            }
        }
        if let Some(end) = self.timespan.1 {
            if timestamp > end {
                return Err(Error::TooLate);
            }
        }

        let db = DATABASE.read().await;
        db.store_event(timestamp, token, x as i32, y as i32, color as i32)
            .await?;
        self.board[y as usize][x as usize] = color;
        self.row_checksums[y as usize] = Self::crc32_u32(&self.board[y as usize]);

        Ok((timestamp, self.checksum()))
    }

    fn crc32_u32(buf: &[u32]) -> u32 {
        let mut hasher = crc32fast::Hasher::new();
        for elem in buf {
            hasher.update(&elem.to_le_bytes());
        }
        hasher.finalize()
    }

    fn checksum(&self) -> u32 {
        let mut hasher = crc32fast::Hasher::new();
        for checksum in &self.row_checksums {
            hasher.update(&checksum.to_le_bytes());
        }
        hasher.finalize()
    }
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(tag = "req", rename_all = "snake_case")]
enum Request {
    Click {
        x: u32,
        y: u32,
        color: u32,
    },
    GetBoard,
    Board(GameState),
    Update {
        x: u32,
        y: u32,
        color: u32,
        timestamp: DateTime<Utc>,
        checksum: Option<u32>,
    },
    PixelPool {
        count: u32,
    },
    Error {
        msg: String,
    },
    GetEvents,
    Events {
        count: usize,
        x: Vec<u32>,
        y: Vec<u32>,
        color: Vec<u32>,
        time_first: DateTime<Utc>,
        time_diff_ms: Vec<u64>,
    },
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct BoardSize {
    x: u32,
    y: u32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Color {
    id: u32,
    hexa: String,
}
