// TODO extract into crate-level module
#[macro_export]
macro_rules! bail {
    ($msg:literal $(,)?) => {
        return core::result::Result::Err(anyhow::anyhow!($msg).into())
    };
    ($err:expr $(,)?) => {
        return core::result::Result::Err(anyhow::anyhow!($err).into())
    };
    ($fmt:expr, $($arg:tt)*) => {
        return core::result::Result::Err(anyhow::anyhow!($fmt, $($arg)*).into())
    };
}
