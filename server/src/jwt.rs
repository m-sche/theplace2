use jsonwebtoken as jwt;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

use crate::bail;
use crate::server::ServerState;

type Result<T, E = anyhow::Error> = core::result::Result<T, E>;

/// JWT claims
#[derive(Debug, Serialize, Deserialize)]
struct Claims {
    sub: String, // username
    iss: String, // issuer
    exp: usize,  // timestamp of expiration
}

pub struct Authenticator {
    jwt_validation: jwt::Validation,
    jwt_decoding_key: jwt::DecodingKey,
}

impl Authenticator {
    pub fn new(issuer: &str, public_key_path: &str) -> Authenticator {
        let issuers: HashSet<String> = vec![issuer.to_string()].into_iter().collect();
        let jwt_validation = jwt::Validation {
            iss: Some(issuers),
            algorithms: vec![jwt::Algorithm::RS256],
            ..Default::default()
        };

        let pubkey = std::fs::read(public_key_path).expect("Public key file not found");
        let jwt_decoding_key =
            jwt::DecodingKey::from_rsa_pem(&pubkey[..]).expect("Wrong public key format");

        Authenticator {
            jwt_validation,
            jwt_decoding_key,
        }
    }

    /// Check if the request contains a valid auth token. Returns the username (user token) extracted from the cookie.
    #[allow(unreachable_code, unused_variables)] // TODO rewrite conditional compilation so this is not needed
    pub fn authenticate(&self, req: &tide::Request<ServerState>) -> Result<String> {
        #[cfg(debug_assertions)]
        return Ok("deadbeef".to_string());

        let config = req.state().config.clone();
        let req_id = &req.header("x-request-id").map_or("???", |v| v.as_str());

        match req.cookie(&config.auth_cookie) {
            Some(cookie) => {
                let provided_token = cookie.value();

                match jwt::decode::<Claims>(
                    &provided_token,
                    &self.jwt_decoding_key,
                    &self.jwt_validation,
                ) {
                    Ok(v) => Ok(v.claims.sub), // Return the username of the authenticated user
                    Err(_) => bail!("Malformed or expired auth token, req_id={}", req_id),
                }
            }
            None => bail!("No auth cookie present, req_id={}", req_id),
        }
    }
}
