use anyhow::{anyhow, Context};
use async_std::sync::{Arc, RwLock};
use std::collections::HashMap;
use std::future::Future;
use std::path::Path;
use std::pin::Pin;
use tera::Tera;
use tide_websockets::{WebSocket, WebSocketConnection};

use crate::bail;
use crate::db::DATABASE;
use crate::game;
use crate::jwt::Authenticator;
use crate::Config;

type Result<T, E = anyhow::Error> = core::result::Result<T, E>;

pub async fn run(config: Arc<Config>) -> Result<()> {
    let server_state = ServerState::new(config.clone()).await?;

    let mut server = tide::with_state(server_state.clone());

    server = add_routes(server, &config)?;
    start_periodic_tasks(&server_state);

    println!(
        "Starting the WebSocket server at addresses {:?}",
        config.addresses
    );
    server.listen(config.addresses.clone()).await?;
    Ok(())
}

fn start_periodic_tasks(server_state: &ServerState) {
    async_std::task::spawn(game::keep_pinging(server_state.clone()));
    async_std::task::spawn(game::keep_sending_pixels(server_state.clone()));
}

/// adds routes and middleware
fn add_routes(
    mut server: tide::Server<ServerState>,
    config: &Arc<Config>,
) -> Result<tide::Server<ServerState>> {
    server.with(authorization_handler).with(logging);

    // TODO specify in config?
    // maybe cobine with ws_url specification?
    server
        .at("/ws")
        .get(WebSocket::new(|request, stream| async move {
            handle_websocket(request, stream).await
        }));
    // the routing is done from the most concrete path to the least, so
    // /dir/foo > /:/foo > /dir/*
    // serve_dir adds * glob at the end
    // TODO specify in config?
    server
        .at("")
        .serve_dir(Path::new(&config.serve_dir).join("static"))
        .context("Problem serving static files")?;
    server
        .at("/")
        .serve_file(Path::new(&config.serve_dir).join("static/index.html"))
        .context("Problem serving index file")?;

    server
        .at("/theplace.js")
        .get(|request: tide::Request<ServerState>| async {
            let context = request.state().tera_context.clone();
            serve_tera(
                request,
                "theplace.js",
                context,
                http_types::mime::JAVASCRIPT,
            )
        });

    Ok(server)
}

async fn theplace_ctx(config: &Arc<Config>) -> tera::Context {
    let mut context = tera::Context::new();
    // TODO consider implementing this via struct serializable serde_json and Context::from_serialize(&product)
    context.insert("ws_url", &config.ws_url);

    let db = DATABASE.read().await;
    let (start, end) = db.get_game_timespan();
    context.insert("start", &start);
    context.insert("end", &end);

    context
}

fn log_time() -> String {
    chrono::Local::now().format("[%F %T]").to_string()
}
macro_rules! print_opt {
    ($pre:expr, $opt:expr, $post:expr) => {{
        let opt: &std::option::Option<&str> = $opt;
        match opt {
            std::option::Option::None => std::string::String::new(),
            std::option::Option::Some(msg) => std::format!("{}{}{}", $pre, msg, $post),
        }
    }};
    ($pre:expr, $opt:expr) => {
        print_opt!($pre, $opt, "")
    };
    ($opt:expr) => {
        print_opt!("", $opt, "")
    };
}

fn logging<'a>(
    request: tide::Request<ServerState>,
    next: tide::Next<'a, ServerState>,
) -> Pin<Box<dyn Future<Output = tide::Result> + Send + 'a>> {
    Box::pin(async {
        let req_id = &request.header("x-request-id").map(|v| v.as_str());
        let Token(token) = request
            .ext()
            .expect("User token not found in request extension");
        println!(
            "{} {} {} user={}{}",
            log_time(),
            request.method().to_string(),
            request.url(),
            token,
            print_opt!(", req_id=", req_id)
        );

        Ok(next.run(request).await)
    })
}

/// Is called once per every new websocket connection
async fn handle_websocket(
    request: tide::Request<ServerState>,
    stream: WebSocketConnection,
) -> tide::Result<()> {
    let req_id = &request.header("x-request-id").map(|v| v.as_str());
    let Token(token) = request
        .ext()
        .expect("User token not found in request extension");

    println!(
        "{} Incoming websocket connection, user={}{}",
        log_time(),
        token,
        print_opt!(", req_id=", req_id)
    );

    if let Err(err) = handle_client(&request, stream).await {
        eprintln!(
            "{} {:?}, user={}{}",
            log_time(),
            err,
            token,
            print_opt!(", req_id=", req_id)
        );
    }

    println!(
        "{} Closing connection, user={}{}",
        log_time(),
        token,
        print_opt!(", req_id=", req_id)
    );

    // AFAIK HTTP status codes are meaningless in WS connection, so we just log the errors and drop the connection
    Ok(())
}

async fn handle_client(
    request: &tide::Request<ServerState>,
    stream: WebSocketConnection,
) -> Result<()> {
    let Token(token) = request.ext().expect(
        "Request extension didn't contain user token. The authorization might be very broken.",
    );

    // TODO move this into ServerState method
    let state = request.state();
    let mut user_list = state.users.write().await;
    let user = user_list
        .get_mut(token)
        .expect("Unauthorized user in privileged context");
    while user.streams.contains_key(&user.next_stream_id) {
        user.next_stream_id += 1;
    }
    let stream_id = user.next_stream_id;
    let mut stream = user.streams.entry(stream_id).or_insert(stream).clone();

    std::mem::drop(user_list); // explicitly drop the lock guard

    // going for connection-level concurrency for now
    // slight redundancy in passing both stream and stream_id, in hopes of less hashmap lookups,
    // locking and more code clarity
    let res = game::run(&mut stream, state, token, stream_id).await;

    // TODO move this into ServerState method
    let state = request.state();
    let mut user_list = state.users.write().await;
    let user = user_list
        .get_mut(token)
        .expect("Unauthorized user in privileged context");
    user.streams.remove(&stream_id);

    Ok(res?)
}

pub struct Token(pub String);

// this is somewhat awkward
// the Middleware trait is implemented by Tide for
// Fn(Request<State>, Next<'a, State>) -> Pin<Box<dyn Future<Output = Result>
// but not for
// Fn(Request<State>, Next<'a, State>) -> impl Future<Output = Result>
// so the async fn notation can only be used only in trait implementations via the async_trait
// (which also results in some extra heap allocations)
// alternatively the trait can be implemented directly on a custom object
//
// examples of both implementations can be seen here:
// https://github.com/http-rs/tide/blob/main/examples/middleware.rs
// example of the async_trait implementation here:
// https://docs.rs/tide/0.16.0/src/tide/log/middleware.rs.html#99-103
fn authorization_handler<'a>(
    mut request: tide::Request<ServerState>,
    next: tide::Next<'a, ServerState>,
) -> Pin<Box<dyn Future<Output = tide::Result> + Send + 'a>> {
    Box::pin(async {
        match authorize(&request).await {
            Ok(token) => {
                request.set_ext(Token(token));
                Ok(next.run(request).await)
            }
            Err(err) => {
                eprintln!("{}", err);
                // Nginx proxy handles returning nice pages - here we just forbid stuff
                Ok(tide::Response::new(tide::StatusCode::Forbidden))
            }
        }
    })
}

/// Checks if the auth cookie is present and contains a known user
async fn authorize(request: &tide::Request<ServerState>) -> Result<String> {
    let user = request.state().authenticator.authenticate(request)?;

    let auth_users = request.state().users.read().await;
    if auth_users.contains_key(&user) {
        return Ok(user);
    }

    bail!("User sent an unauthorized token \"{}\"", user)
}

impl ServerState {
    pub async fn new(config: Arc<Config>) -> Result<Self> {
        println!("Building the ServerState");

        // ../ does not work with Tera if it traverses above the topmost used directory (bug?)
        // blah/../foo/* works
        // ../blah/foo/* doesn't work
        // thus we need to canonicalize
        let mut path = Path::new(&config.serve_dir)
            .join("templates")
            .canonicalize()?;
        path = path.join("**/*");
        let tera = match Tera::new(
            path.to_str()
                .ok_or_else(|| anyhow!("The template path is not a valid unicode"))?,
        ) {
            Ok(tera) => tera,
            Err(err) => {
                bail!("UI template parsing error: {}", err);
            }
        };

        // extracted awaits out, so the order of execution is explicit
        let game = Arc::new(RwLock::new(game::GameState::new().await?));
        let tera_context = Arc::new(theplace_ctx(&config).await);
        let server_state = ServerState {
            game,
            users: Arc::new(RwLock::new(HashMap::new())),
            config: config.clone(),
            tera: Arc::new(tera),
            tera_context,
            authenticator: Arc::new(Authenticator::new(&config.jwt_issuer, &config.jwt_pubkey)),
        };

        let db = DATABASE.read().await;
        let user_tokens = db.get_users().await?;
        for token in user_tokens {
            let user = User {
                streams: HashMap::<u32, WebSocketConnection>::new(),
                next_stream_id: 0,
                pixel_pool: 0,
            };
            server_state.users.write().await.insert(token, user);
        }

        Ok(server_state)
    }
}

fn serve_tera(
    request: tide::Request<ServerState>,
    template: &str,
    context: Arc<tera::Context>,
    mimetype: http_types::Mime,
) -> tide::Result {
    let tera = &request.state().tera;

    let body = match tera.render(&template, &context) {
        Ok(body) => body,
        Err(err) => {
            eprintln!("{}", &err);
            // TODO crash with 404 if err == NotFound
            // TODO this should maybe crash harder, or somehow propagate to the standard error
            // channels
            // furthermore I still have no idea, how the following two lines differ during
            // execution:
            //return tide::Error::new(tide::StatusCode::InternalServerError, err),
            return Ok(tide::Response::new(tide::StatusCode::InternalServerError));
        }
    };
    Ok(tide::Response::builder(tide::StatusCode::Ok)
        .content_type(mimetype)
        .body(body)
        .build())
}

// server state is cloneable, because it only contains Arcs
// this is especially important for periodic tasks, so keep it this way, or rewrite entire codebase
// to use Arc<ServerState> instead
#[derive(Clone)]
pub struct ServerState {
    pub game: Arc<RwLock<game::GameState>>,
    pub users: Arc<RwLock<HashMap<String, User>>>,
    pub config: Arc<Config>,
    tera: Arc<Tera>,
    tera_context: Arc<tera::Context>,
    authenticator: Arc<Authenticator>,
}

pub struct User {
    pub streams: HashMap<u32, WebSocketConnection>,
    next_stream_id: u32,
    pub pixel_pool: u32,
}
