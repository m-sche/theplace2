use anyhow::{Context, Result};
use db::DATABASE;
use serde::Deserialize;
use std::fs::read_to_string;
use std::sync::Arc;

mod db;
mod error;
mod game;
mod jwt;
mod server;

#[async_std::main]
async fn main() -> Result<()> {
    // on panic terminate all threads
    let default_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        default_hook(panic_info);
        std::process::exit(101);
    }));

    println!("Loading configuration");
    let config = Config::new()?;

    println!("Connecting to the database, game \"{}\"", config.game_name);
    DATABASE
        .write()
        .await
        .init(&config.database_url, &config.game_name)
        .await?;

    server::run(Arc::new(config))
        .await
        .context("Can't run the server")?;

    Ok(())
}

#[derive(Deserialize, Debug)]
pub struct Config {
    database_url: String,
    game_name: String,
    addresses: Vec<String>,
    auth_cookie: String,
    jwt_pubkey: String,
    jwt_issuer: String,
    ws_url: String,
    serve_dir: String,
    ping_interval: u64,
    pixel_interval: u64,
    pixel_limit: u32,
}

impl Config {
    const CONFIG: &'static str = "config.toml";
    fn new() -> Result<Self> {
        let config: Config = toml::from_str(
            &read_to_string(Self::CONFIG)
                .context(format!("Can't read the config file at {}", Self::CONFIG))?,
        )
        .context(format!("Can't read the config file at {}", Self::CONFIG))?;

        Ok(config)
    }
}
