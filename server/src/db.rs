use anyhow::Context;
use async_std::sync::RwLock;
//use sqlx::prelude::*;
use chrono::prelude::*;
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgPool;
use thiserror::Error;

type Result<T, E = Error> = core::result::Result<T, E>;

pub struct Database {
    pool: Option<sqlx::PgPool>,
    game: Game,
}

// RwLock instead of Mutex allows for many readers or one writer
// however it might get problematic if it's insides are Send and
// not Sync and the database access needs to be Sync
pub static DATABASE: RwLock<Database> = RwLock::new(Database::new());

#[derive(sqlx::FromRow, Debug)]
pub struct Color {
    pub id: i32,
    pub hexa: String,
}

#[derive(Debug)]
pub struct Colors {
    pub colors: Vec<Color>,
    pub background: i32,
}

#[derive(sqlx::FromRow, Debug)]
struct Game {
    id: i32,
    name: String,
    bgr_color: i32,
    board_width: i32,
    board_height: i32,
    start_time: Option<DateTime<Utc>>,
    end_time: Option<DateTime<Utc>>,
}

// Fucking stable rust doesn't have constant Default
// probably blocked by https://github.com/rust-lang/rust/issues/67792
impl Game {
    const fn new() -> Game {
        Game {
            id: 0,
            name: String::new(),
            bgr_color: 0,
            board_width: 0,
            board_height: 0,
            start_time: None,
            end_time: None,
        }
    }
}

#[derive(Debug)]
pub struct BoardSize {
    pub x: i32,
    pub y: i32,
}

#[derive(sqlx::FromRow, Debug)]
pub struct Pixel {
    pub x: i32,
    pub y: i32,
    pub color: i32,
    pub timestamp: DateTime<Utc>,
}

#[derive(Debug)]
pub struct Board {
    pub size: BoardSize,
    pub board: Vec<Vec<i32>>,
    pub timestamp: DateTime<Utc>, // Timestamp of latest event

    // The game lasts from start_time to end_time
    pub start_time: Option<DateTime<Utc>>,
    pub end_time: Option<DateTime<Utc>>,
}

impl Database {
    const fn new() -> Self {
        Database {
            pool: None,
            game: Game::new(),
        }
    }

    pub async fn init(&mut self, db_url: &str, game_name: &str) -> Result<()> {
        self.pool = Some(PgPool::connect(db_url).await.context(format!(
            "Could not connect to the database with connection string \"{}\"",
            db_url
        ))?);

        let mut conn = self.connection().await?;

        let query = "SELECT * FROM game WHERE name = $1";
        self.game = sqlx::query_as::<_, Game>(query)
            .bind(game_name)
            .fetch_one(&mut conn)
            .await
            .context("Can't fetch the game data")?;

        Ok(())
    }

    async fn connection(&self) -> Result<PoolConnection<sqlx::Postgres>> {
        // XXX this will crash entire server, if the connection times out
        // is this desired?
        let ret = self
            .pool
            .as_ref()
            .context("Attempted to use Database before initialization")?
            .acquire()
            .await
            .map_err(Error::NotConnected)?;

        Ok(ret)
    }

    #[allow(dead_code)]
    pub async fn is_authorized(&self, token: &str) -> Result<bool> {
        let mut conn = self.connection().await?;

        let query = "SELECT 1 as r FROM \"user\" WHERE game = $1 and token = $2";
        let ret = sqlx::query(query)
            .bind(self.game.id)
            .bind(token)
            .fetch_optional(&mut conn)
            .await
            .context("Can't fetch the authorization data")?
            .is_some();

        Ok(ret)
    }

    #[allow(dead_code)]
    pub async fn get_colors(&self) -> Result<Colors> {
        let mut conn = self.connection().await?;

        let query = "SELECT id, hexa FROM color";
        let colors = sqlx::query_as::<_, Color>(query)
            .fetch_all(&mut conn)
            .await
            .context("Can't fetch the colors")?;

        let ret = Colors {
            colors,
            background: self.game.bgr_color,
        };

        Ok(ret)
    }

    #[allow(dead_code)]
    pub async fn get_board(&self) -> Result<Board> {
        let mut conn = self.connection().await?;

        let query = "
        WITH numbered AS (
            SELECT x, y, color, \"timestamp\", ROW_NUMBER() OVER (
                PARTITION BY x, y ORDER BY \"timestamp\" DESC
            ) AS row_num
            FROM \"event\"
            WHERE game = $1
            GROUP BY x, y, color, \"timestamp\"
        )
        SELECT x, y, color, \"timestamp\"
        FROM numbered
        WHERE row_num = 1
        ";
        let pixels = sqlx::query_as::<_, Pixel>(query)
            .bind(self.game.id)
            .fetch_all(&mut conn)
            .await
            .context("Can't fetch the pixels")?;

        let mut board = vec![
            vec![self.game.bgr_color; self.game.board_width as usize];
            self.game.board_height as usize
        ];

        for pixel in &pixels {
            let x = pixel.x as usize;
            let y = pixel.y as usize;
            board[y][x] = pixel.color;
        }

        // Get maximum of pixel.timestamp
        let last_timestamp =
            pixels
                .iter()
                .fold(self.game.start_time.unwrap_or(Utc::now()), |max, pixel| {
                    if pixel.timestamp >= max {
                        pixel.timestamp
                    } else {
                        max
                    }
                });

        let ret = Board {
            size: BoardSize {
                x: self.game.board_width,
                y: self.game.board_height,
            },
            board,
            timestamp: last_timestamp,
            start_time: self.game.start_time,
            end_time: self.game.end_time,
        };

        Ok(ret)
    }

    #[allow(dead_code)]
    pub async fn store_event(
        &self,
        timestamp: DateTime<Utc>,
        user: &str,
        x: i32,
        y: i32,
        color: i32,
    ) -> Result<()> {
        let mut conn = self.connection().await?;

        let query = "
        INSERT INTO \"event\" (\"timestamp\", game, \"user\", x, y, color)
        VALUES ($1, $2, $3, $4, $5, $6)
        ";
        sqlx::query(query)
            .bind(timestamp)
            .bind(self.game.id)
            .bind(user)
            .bind(x)
            .bind(y)
            .bind(color)
            .execute(&mut conn)
            .await
            .context("Can't store event")?;

        Ok(())
    }

    #[allow(dead_code)]
    pub async fn get_users(&self) -> Result<Vec<String>> {
        let mut conn = self.connection().await?;

        let query = "SELECT token FROM \"user\" WHERE game = $1";
        let ret = sqlx::query_scalar(query)
            .bind(self.game.id)
            .fetch_all(&mut conn)
            .await
            .context("Can't fetch table of users")?;

        Ok(ret)
    }

    #[allow(dead_code)]
    pub async fn get_events(&self) -> Result<Vec<Pixel>> {
        let mut conn = self.connection().await?;

        let query = "SELECT * FROM \"event\" WHERE game = $1 ORDER BY \"timestamp\"";
        let pixels = sqlx::query_as::<_, Pixel>(query)
            .bind(self.game.id)
            .fetch_all(&mut conn)
            .await
            .context("Can't fetch the pixels")?;

        Ok(pixels)
    }

    #[allow(dead_code)]
    pub fn get_game_timespan(&self) -> (Option<DateTime<Utc>>, Option<DateTime<Utc>>) {
        (self.game.start_time, self.game.end_time)
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Can't acquire database connection: {0}")]
    NotConnected(#[source] sqlx::Error),
    /*
    NotConnected {
        source: sqlx::Error
    },
    */
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}
