import {Game, Animation} from './gamelib.js'

const element_ids = {
	canvas: 'canvas',
	colors: 'colors',
	pixel_pool: 'pp_num',
	errors: 'errors',
	ws_status: 'ws_status',
}

const CONFIG = {
	element_ids,
	ws_url: "{{ ws_url | safe }}",
	start_time: new Date("{{ start | safe }}"),
	end_time: new Date("{{ end | safe }}"),
}

const urlparams = new URLSearchParams(window.location.search);

let game
if (urlparams.has('animation'))
	game = new Animation(CONFIG, urlparams.get('animation'))
else
	game = new Game(CONFIG)
game.run()
