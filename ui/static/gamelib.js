const WS_RECONNECT_DELAY = 10_000	// ms
const MAX_WS_RETRIES = 6		// Until WS endpoint is probed / full page reload is forced
const ERROR_MSG_TIMEOUT = 5_000		// ms
const ANIM_FINAL_DELAY = 10_000		// ms, delay after full replay
const CHECKSUM_MARGIN = 1_000		// ms, for how long can bard be in an inconsistent state

const PIX_SIZE = 20	// How big is one edge of a pixel (in pixels)
const PIX_BORDER = 1	// Pixel border width

if (typeof msgpack == 'undefined')
	console.error('Msgpack library is not imported')


function precompute_crc_table() {
	let c
	let table = []
	for (let n = 0; n < 256; n++) {
		c = n
		for (let k = 0; k < 8; k++)
			c = (c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1)
		table[n] = c
	}
	return table
}

const CRC_TABLE = precompute_crc_table();


// Serialize and deserialize msgpack messages over websocket
class WebsocketAPI {
	constructor(game, ws_url, on_msg, on_status) {
		this.ws_url = ws_url
		this.ws = null
		this.game = game
		this.connected = false

		this.on_msg = on_msg || console.log
		this.on_status = on_status || (s => {})

		this.retried = 0
	}

	async connect() {
		let self = this
		return new Promise((resolve, reject) => {
			self.ws = new WebSocket(self.ws_url)
			self.ws.addEventListener('open', () => {
				this.connected = true
				this.on_status(this.connected)
				this.retried = 0
				resolve()
			})
			self.ws.addEventListener('message', self.receive.bind(self))
			/*self.ws.addEventListener('error', err => {
				this.on_status('offline')
				self.game.errorbar.error(err)
			})*/
			self.ws.addEventListener('close', ev => {
				this.connected = false
				this.on_status(this.connected)
				self.game.errorbar.error('WS connection closed, reconnecting', ev.code)
				window.setTimeout(async () => {
					this.retried++
					if (this.retried >= MAX_WS_RETRIES) {
						try {
							let probe = await fetch(window.location.href)
							console.debug('Probe HTTP status:', probe.status)

							// Cookie expired / website moved -> reload
							if (!probe.ok)
								window.location.href = window.location.href
						} catch (err) {
							// We don't mind if there's no connectivity
							// In that case we keep trying to connect
						}
						this.retried = 0
					}
					await self.connect()
				}, WS_RECONNECT_DELAY)
			})
		})
	}

	// Encode and send data over websocket
	send(data) {
		if (this.ws) {
			console.debug(data)
			this.ws.send(msgpack.serialize(data))
		} else {
			this.game.errorbar.error('Cannot send request, WS not initialzied')
		}
	}

	// Called when are received over websocket and decodes them
	async receive(ev) {
		let buffer = await ev.data.arrayBuffer()
		let msg = msgpack.deserialize(buffer)
		this.on_msg(msg)
	}
}


class Canvas {
	constructor(game, element_id, interactive = true) {
		this.canvas = document.getElementById(element_id)
		this.game = game

		if (!this.canvas.getContext) {
			console.error("Please use a canvas-supporting web browser")
			return;
		}
		this.ctx = this.canvas.getContext('2d')

		if (interactive) {
			this.canvas.addEventListener('mousemove', this.set_highlight.bind(this));
			this.canvas.addEventListener('mouseout', this.clear_highlight.bind(this));
			this.canvas.addEventListener('click', this.click.bind(this));
		}

		this.highlighted = null	// {x, y} coordinates of pixel hovered by mouse
	}

	init() {
		this.ctx.canvas.width = this.game.size.x * PIX_SIZE
		this.ctx.canvas.height = this.game.size.y * PIX_SIZE

		// Border color
		this.ctx.fillStyle = 'lightgray'
		this.ctx.fillRect(0, 0, this.game.size.x * PIX_SIZE, this.game.size.y * PIX_SIZE)

		for (let y = 0; y < this.game.size.y; y++)
			for (let x = 0; x < this.game.size.x; x++)
				this.draw_pixel(x, y, this.game.board[y][x])
	}

	draw_pixel(x, y, color_id) {
		this.ctx.fillStyle = this.game.colorbar.hexa(color_id)

		this.ctx.fillRect(
			(x * PIX_SIZE) + PIX_BORDER,
			(y * PIX_SIZE) + PIX_BORDER,
			PIX_SIZE - 2 * PIX_BORDER,
			PIX_SIZE - 2 * PIX_BORDER
		)
	}

	click(ev) {
		// This game does not want any clicks (e.g. animation)
		if (typeof this.game.click === undefined) {
			this.clear_highlight(null)
			return
		}

		// Game hasn't been started yet or is already over
		let now = new Date()
		if (now < this.game.config.start_time || now > this.game.config.end_time) {
			this.game.errorbar.error('This game is closed')
			this.clear_highlight(null)
			return
		}

		// No WS connection
		if (!this.game.api.connected) {
			this.game.errorbar.warning('Not conected to the server')
			this.clear_highlight(null)
			return
		}

		// Don't even try if we don't have free pixels
		if (this.game.pixel_pool < 1) {
			this.game.errorbar.warning('You have no pixels left')
			this.clear_highlight(null)
			return
		}

		// Check if any color is selected
		if (this.game.colorbar.selected === null) {
			this.game.errorbar.warning('Select a color before drawing')
			return
		}

		let [x, y] = this.relative_xy(ev)

		// Clicking on right or bottom border causes off-by-one error
		if (x >= this.game.size.x || y >= this.game.size.y)
			return

		// Prevent wasteful drawing of a pixel with the same color as what's on the board
		if (this.game.board[y][x] === this.game.colorbar.selected) {
			this.game.errorbar.warning('Prevented accidental click')
			return
		}

		this.game.click(x, y, this.game.colorbar.selected)
	}

	set_highlight(ev) {
		// Check if any color is selected
		if (this.game.colorbar.selected === null)
			return

		let [x, y] = this.relative_xy(ev)

		if ((this.highlighted === null ||
		    x != this.highlighted.x ||
		    y != this.highlighted.y) &&
		    this.game.colorbar.colors.length > 0 &&
		    x < this.game.size.x && y < this.game.size.y) {
			this.clear_highlight()
			this.draw_pixel(x, y, this.game.colorbar.selected)
			this.highlighted = {x, y}
		}
	}

	clear_highlight(ev) {
		if (this.highlighted && this.game.colorbar.colors.length > 0) {
			let x = this.highlighted.x
			let y = this.highlighted.y
			this.draw_pixel(x, y, this.game.board[y][x])
			this.highlighted = null
		}
	}

	// Convert click/mousemove event to logical coordinates
	relative_xy(ev)
	{
		if (ev.offsetX) {
			var rel_x = ev.offsetX;
			var rel_y = ev.offsetY;
		} else {
			// Old browsers
			let target = ev.target;
			let rect = target.getBoundingClientRect();

			var rel_x = ev.clientX - rect.left;
			var rel_y = ev.clientY - rect.top;
		}

		let x = Math.round(rel_x * this.game.size.x / this.canvas.clientWidth - 0.5);
		let y = Math.round(rel_y * this.game.size.y / this.canvas.clientHeight - 0.5);

		return [x, y]
	}
}


class ErrorBar {
	constructor(element_id) {
		this.root = document.getElementById(element_id)
	}

	// Not meant to be called outside of ErrorBar, use error() or warning()
	show(type, ...msg) {
		let errormsg = document.createElement('div')
		errormsg.classList.add(type)
		errormsg.innerHTML = msg.map(x => {
			if (typeof x == 'object')
				return JSON.stringify(x).replace(',', ',<br>\n')
			else
				return x
		}).join('<br>\n') + '<hr>\nTap to dismiss'

		errormsg.addEventListener('click', ev => {
			errormsg.remove();
		})

		window.setTimeout(() => errormsg.remove(), ERROR_MSG_TIMEOUT)

		this.root.appendChild(errormsg)

	}

	error(...msg) {
		console.error(...msg)
		this.show('error', ...msg)
	}

	warning(...msg) {
		console.warn(...msg)
		this.show('warning', ...msg)
	}
}

class ColorBar {
	constructor(game, element_id) {
		this.root = document.getElementById(element_id)
		this.selected = null
		this.color_list = []
		this.game = game
	}

	set colors(color_list) {
		this.color_list = color_list

		let buttons = this.color_list.map(color => {
			let b = document.createElement('button')
			b.classList.add('color')
			b.style.background = '#' + color.hexa
			b.id = 'color_' + color.id
			b.dataset.id = color.id

			if (color.id === this.selected)
				b.classList.add('selected')

			b.addEventListener('mousedown', ev => {
				if (this.selected !== null) {
					let old = document.getElementById('color_' + this.selected)
					old.classList.remove('selected')
				}
				this.selected = parseInt(ev.target.dataset.id)
				ev.target.classList.add('selected')
			})

			return b
		})
		this.root.replaceChildren(...buttons)
	}

	get colors() {
		return this.color_list
	}

	// Return array index in color list for given color id
	index(color_id) {
		return this.color_list.map(c => c.id).indexOf(color_id)
	}

	// Return hexa value including leading # for a specified color id
	hexa(color_id) {
		let ix = this.index(color_id)
		if (ix < 0)
			this.game.errorbar.error('Unknown color id', color_id)
		else
			return '#' + this.color_list[ix].hexa;
	}
}

// Main entrypoint for The place is Game.run(), which initializes all other modules
class Game {
	constructor(config) {
		this.config = config
		this.errorbar = new ErrorBar(this.config.element_ids.errors)
		this.api = new WebsocketAPI(this,
			this.config.ws_url,
			this.receive.bind(this),
			this.wsstatus.bind(this)
		)
		this.colorbar = new ColorBar(this, config.element_ids.colors)
		this.canvas = new Canvas(this, config.element_ids.canvas)

		this.size = {x: 0, y: 0}
		this.board = [[]]
		this.pixel_pool = 0

		this.board_ts = null	// Date of the last board update

		this.checksum = new HashBoard()
		this.cksum_timer = null
	}

	async run() {
		await this.api.connect()

		// Ask for board again if we received none
		window.setTimeout(async () => {
			if (this.board.length == 0)
				this.api.send({req: 'get_board'})
		}, 5_000)
	}

	click(x, y, color_id) {
		this.canvas.draw_pixel(x, y, color_id)
		this.board[y][x] = color_id;
		this.api.send({req: 'click', x, y, color: color_id})
		this.pixel_pool--	// Presume successful click
		this.show_pixel_pool()
		this.checksum.click(y)
	}

	show_pixel_pool(count) {
		let element = document.getElementById(this.config.element_ids.pixel_pool)
		element.textContent = this.pixel_pool
	}

	receive(msg) {
		console.debug(msg)

		switch (msg.req) {
		case 'board':
			this.board = msg.board
			this.size = msg.size
			this.colorbar.colors = msg.colors
			this.board_ts = new Date(msg.timestamp)
			this.canvas.init()
			this.checksum.init(this.board)
			break

		case 'pixel_pool':
			this.pixel_pool = msg.count
			this.show_pixel_pool()
			break

		case 'update':
			if (this.colorbar.index(msg.color) < 0 ||
			    msg.x < 0 || msg.y < 0 ||
			    msg.x >= this.size.x || msg.y >= this.size.y) {
				this.errorbar.error('Got pixel update outside of the board')
				this.api.send({req: 'get_board'})
				break
			}

			this.board[msg.y][msg.x] = msg.color
			this.board_ts = msg.timestamp

			// Compute new checksum and compare with update message
			this.checksum.click(msg.y, this.board)
			let cksum_ok = this.checksum.matches(msg.checksum)

			// If we are not consistent, start the margin period before error
			if (!cksum_ok && !this.cksum_timer) {
				console.error(`CRC error: ${this.checksum.crc} != ${msg.checksum}`)
				this.cksum_timer = setTimeout(() => {
					this.errorbar.error('Board is inconsistent, resetting')
					this.api.send({req: 'get_board'})
				}, CHECKSUM_MARGIN)
			}

			// If we are consistent again, clear timer
			if (cksum_ok && this.cksum_timer)
				clearTimeout(this.cksum_timer)

			this.canvas.draw_pixel(msg.x, msg.y, msg.color)
			break

		case 'error':
			this.errorbar.error(msg.msg)
			break

		default:
			this.errorbar.error('Received unknown msgpack message', msg)
		}
	}

	wsstatus(st) {
		let element = document.getElementById(this.config.element_ids.ws_status)
		if (element)
			element.className = st ? 'online' : 'offline'
	}
}

class Crc32 {
	constructor() {
		this.state = -1
	}

	update_byte(byte) {
		this.state = (this.state >>> 8) ^ CRC_TABLE[(this.state ^ byte) & 0xff]
	}

	update_word(word) {
		for (let i = 0; i < 4; i++) {
			this.update_byte(word & 0xff)
			word >>>= 8;
		}
	}

	finalize() {
		return (this.state ^ (-1))
	}
}

class HashBoard {
	constructor() {
		this.precomputed = []
		this.board = null
	}

	get crc() {
		let hasher = new Crc32()
		for (let y = 0; y < this.board.length; y++)
			hasher.update_word(this.precomputed[y])
		return hasher.finalize()
	}

	init(board) {
		this.board = board
		for (let y = 0; y < this.board.length; y++)
			this.click(y)
	}

	click(y) {
		let hasher = new Crc32();
		for (let x = 0; x < this.board[y].length; x++)
			hasher.update_word(this.board[y][x])
		this.precomputed[y] = hasher.finalize()
	}

	matches(other) {
		// Convert to signed integer and compare
		return other !== null && (other | 0) === this.crc
	}
}

// Used instead of Game if we only replay animation
class Animation {
	constructor(config, anim_delay = 50) {
		this.config = config
		this.errorbar = new ErrorBar(this.config.element_ids.errors)
		this.api = new WebsocketAPI(this,
			this.config.ws_url,
			this.receive.bind(this),
			this.wsstatus.bind(this)
		)
		this.colorbar = new ColorBar(this, this.config.element_ids.colors)
		this.canvas = new Canvas(this, this.config.element_ids.canvas, false)

		this.board = [[]]
		this.size = {x: 0, y: 0}
		this.events = []
		this.anim_delay = anim_delay
	}

	async run() {
		await this.api.connect()

		// Ask for board again if we received none
		window.setTimeout(async () => {
			if (this.board.length == 0)
				this.api.send({req: 'get_board'})
		}, 5_000)
	}

	receive(msg) {
		switch (msg.req) {
		case 'board':
			this.board = msg.board
			this.size = msg.size
			this.colorbar.colors = msg.colors
			this.background = msg.background

			this.api.send({req: 'get_events'})
			break

		case 'events':
			this.events = []
			let timestamp = new Date(msg.time_first)
			for (let i = 0; i < msg.count; i++) {
				timestamp = new Date(timestamp.getTime() + msg.time_diff_ms[i])
				let ev = {x: msg.x[i], y: msg.y[i], color: msg.color[i], timestamp}
				this.events.push(ev)
			}
			this.animate()
			break

		case 'error':
			this.errorbar.error(msg.msg)
			break
		}
	}

	wsstatus(st) {
		let element = document.getElementById(this.config.element_ids.ws_status)
		if (element)
			element.className = st ? 'online' : 'offline'
	}

	async animate() {
		// Start with empty canvas
		for (let y = 0; y < this.size.y; y++)
			for (let x = 0; x < this.size.x; x++)
				this.board[y][x] = this.background
		this.canvas.init()

		for (let ev of this.events) {
			if (this.colorbar.index(ev.color) < 0 ||
			    ev.x < 0 || ev.y < 0 ||
			    ev.x >= this.size.x || ev.y >= this.size.y) {
				this.errorbar.error('Got pixel update outside of the board')
				continue
			}

			this.board[ev.y][ev.x] = ev.color
			this.board_ts = ev.timestamp

			this.canvas.draw_pixel(ev.x, ev.y, ev.color)
			await new Promise(resolve => setTimeout(resolve, this.anim_delay));	// Delay
		}

		setTimeout(this.animate.bind(this), ANIM_FINAL_DELAY)
	}
}

export { Game, Animation }
