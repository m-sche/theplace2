-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2
-- PostgreSQL version: 11.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: theplace | type: DATABASE --
-- -- DROP DATABASE IF EXISTS theplace;
-- CREATE DATABASE theplace;
-- -- ddl-end --
-- 

-- object: public.game | type: TABLE --
-- DROP TABLE IF EXISTS public.game CASCADE;
CREATE TABLE public.game (
	id serial NOT NULL,
	name varchar NOT NULL,
	bgr_color integer NOT NULL,
	board_width integer NOT NULL,
	board_height integer NOT NULL,
	start_time timestamptz,
	end_time timestamptz,
	CONSTRAINT game_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.game OWNER TO postgres;
-- ddl-end --

-- object: public."user" | type: TABLE --
-- DROP TABLE IF EXISTS public."user" CASCADE;
CREATE TABLE public."user" (
	game integer NOT NULL,
	token text NOT NULL,
	name text,
	CONSTRAINT user_pk PRIMARY KEY (token,game)

);
-- ddl-end --
-- ALTER TABLE public."user" OWNER TO postgres;
-- ddl-end --

-- object: public.event | type: TABLE --
-- DROP TABLE IF EXISTS public.event CASCADE;
CREATE TABLE public.event (
	id serial NOT NULL,
	"timestamp" timestamptz NOT NULL DEFAULT NOW(),
	game integer NOT NULL,
	"user" text NOT NULL,
	x integer NOT NULL,
	y integer NOT NULL,
	color integer NOT NULL,
	CONSTRAINT event_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.event OWNER TO postgres;
-- ddl-end --

-- object: public.color | type: TABLE --
-- DROP TABLE IF EXISTS public.color CASCADE;
CREATE TABLE public.color (
	id serial NOT NULL,
	hexa varchar(8) NOT NULL,
	name text,
	CONSTRAINT color_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.color OWNER TO postgres;
-- ddl-end --

-- object: game_bgr_color_fk | type: CONSTRAINT --
-- ALTER TABLE public.game DROP CONSTRAINT IF EXISTS game_bgr_color_fk CASCADE;
ALTER TABLE public.game ADD CONSTRAINT game_bgr_color_fk FOREIGN KEY (bgr_color)
REFERENCES public.color (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE;
-- ddl-end --

-- object: user_game_fk | type: CONSTRAINT --
-- ALTER TABLE public."user" DROP CONSTRAINT IF EXISTS user_game_fk CASCADE;
ALTER TABLE public."user" ADD CONSTRAINT user_game_fk FOREIGN KEY (game)
REFERENCES public.game (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE;
-- ddl-end --

-- object: event_user_fk | type: CONSTRAINT --
-- ALTER TABLE public.event DROP CONSTRAINT IF EXISTS event_user_fk CASCADE;
ALTER TABLE public.event ADD CONSTRAINT event_user_fk FOREIGN KEY (game,"user")
REFERENCES public."user" (game,token) MATCH FULL
ON DELETE NO ACTION ON UPDATE CASCADE;
-- ddl-end --

-- object: event_color_fk | type: CONSTRAINT --
-- ALTER TABLE public.event DROP CONSTRAINT IF EXISTS event_color_fk CASCADE;
ALTER TABLE public.event ADD CONSTRAINT event_color_fk FOREIGN KEY (color)
REFERENCES public.color (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


